package main

import (
	"flag"
	"fmt"
	"math/big"
	"os"
)

var filename string

func main() {
	flag.StringVar(&filename, "filename", "", "filename containing the hex value")
	flag.Parse()
	if filename == "" {
		fmt.Println("filename flag required")
		os.Exit(1)
	}

	f, err := os.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	b := new(big.Int)
	b.SetString(string(f), 16)

	fmt.Println(b.String())
}
